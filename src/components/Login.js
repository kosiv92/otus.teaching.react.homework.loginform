import React, { useState } from 'react';

function Login()
{    
    const url = "https://localhost:6001/api/Account/Login";    
    
    const [userData, setUserData] = useState(
        {
            username: '',
            password: ''
        }
    );
    
    const setUsername = (event) =>
    {
        setUserData((previousState) =>
        {
            return{
                ...previousState,
                username: event.target.value                
            }            
        })        
    };

    const setPassword = (event) =>
    {
        setUserData((previousState) =>
        {
            return{
                ...previousState,
                password: event.target.value
            }            
        })           
    };

    const submitHandler = (event) => {
        event.preventDefault();        
        const response = fetch(url, {
        method: 'POST',        
        headers: {
            'Content-Type': 'application/json'
        },        
        body: JSON.stringify(userData)});
    if(response.ok){
        alert('Успех!');
    }else{
        alert("Вход не удался!");
    }
        setUserData({username: '', password: ''});    
    };
            
    return (        
        <form className='authorization'
        onSubmit={submitHandler}>            
            <div>
                <input  placeholder='Input your login' 
                    type='text'
                    value={userData.username}
                    onChange={setUsername}/>
                    </div>
            <div>
            <input  placeholder='Input your password'
                    type='password'
                    value={userData.password}
                    onChange={setPassword}/>
                    <br/>
            </div>            
            <button type='submit'>Login</button>
            <br/>            
        </form>        
    );
}

export default Login;